package lab1;

import java.util.Scanner;

public class EncryptDecryptTest {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.println("Enter key: ");
        var key = scanner.nextLine();

        System.out.println("Enter message: ");
        var original = scanner.nextLine();
        var aes = Aes.encrypt(original, key);
        var rsa = Rsa.encrypt(original);

        System.out.println("AES-encrypted message: " + aes);
        System.out.println("AES-decrypted message: " + Aes.decrypt(aes, key));
        System.out.println("RSA-encrypted message: " + rsa);
        System.out.println("RSA-decrypted message: " + Rsa.decrypt(rsa));
    }
}
