package lab2;

import java.nio.charset.StandardCharsets;
import java.security.*;

public class Dsa {
    private static PrivateKey privateKey;
    private static PublicKey publicKey;

    public static Signature makeSignature() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
            keyGen.initialize(1024, new SecureRandom());
            KeyPair pair = keyGen.generateKeyPair();
            var signature = Signature.getInstance("SHA/DSA");
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();
            signature.initSign(privateKey);
            return signature;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] sign(String message, Signature signature) {
        try {
            byte[] messageToBytes = message.getBytes(StandardCharsets.UTF_8);
            signature.update(messageToBytes);
            return signature.sign();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean verify(String message, byte[] signed, Signature signature) {
        try {
            byte[] messageToBytes = message.getBytes(StandardCharsets.UTF_8);
            signature.initVerify(publicKey);
            signature.update(messageToBytes);
            return signature.verify(signed);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
