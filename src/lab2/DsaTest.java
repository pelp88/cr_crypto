package lab2;

import java.util.Arrays;
import java.util.Scanner;

public class DsaTest {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.println("Enter message: ");
        var original = scanner.nextLine();
        var signature = Dsa.makeSignature();
        var dsa = Dsa.sign(original, signature);

        System.out.println("Message: " + original);
        System.out.println("Signature: " + Arrays.toString(dsa));
        System.out.println("Verified?: " + Dsa.verify(original, dsa, signature));
    }
}
